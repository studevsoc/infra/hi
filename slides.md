---
# try also 'default' to start simple
theme: light-icons
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
background: https://source.unsplash.com/collection/94734566/1920x1080
# apply any windi css classes to the current slide
class: 'text-center'
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# show line numbers in code blocks
lineNumbers: false
# some information about the slides, markdown enabled
info: |
  ## Student Developers Society 
   We help people, creating more people who help people

  Learn more at [Sli.dev](https://sli.dev)
# persist drawings in exports and build
drawings:
  persist: false
---
 
<div class="my-20">
    <h1>Student Developers Society</h1>
    <h3>We help people, creating more people who help people</h3>
    <img src="https://gitlab.com/studevsoc/logistics/-/raw/master/logodark.png" alt="SDS Logo" style="height: 2.5rem; margin: 10vh auto 0 auto"/>
</div>
<div class="pt-2">
  <span @click="$slidev.nav.next" class="px-2 py-1 rounded cursor-pointer" hover="bg-white bg-opacity-10">
    Press Space for next page <carbon:arrow-right class="inline"/>
  </span>
</div>


<!--
The last comment block of each slide will be treated as slide notes. It will be visible and editable in Presenter Mode along with the slide. [Read more in the docs](https://sli.dev/guide/syntax.html#notes)
-->

---
layout: image-right
image: https://studevsoc.com/static/img/developers.svg
---
# Core ideals
- ✨ Being excellent to everyone
- 🛠 Hacker Ethic 
- ⚖ Doing things simply because it's the right thing to do ️
- 💪 Self Reliance

---

<div grid="~ cols-2 gap-2">
<div>
<h2>What we do:</h2>
<ul>
<li>Meetups</li>
<li>Skill enhancement workshops</li>
<li>Software Development</li>
</ul>
<h2>Events Organized:</h2>
<ul>
<li>Rails Girls Kochi, 2018</li>
<li>Debutsav Kochi, 2018</li>
<li>Designers Fest, 2019</li>
<li>LetsPy, 2019</li>
</ul>
</div>
<div>
<Tweet id="1420788123856891904" scale="0.55" />
</div>
</div>

---
layout: center
class: text-center
---

# We're stronger together!

[About Us](https://studevsoc.com/about) · [Contact Us](mailto:contact@studevsoc.com)
